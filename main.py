#!/usr/bin/python3

#Version: 1
# Date:- 23/10/2022

import requests
from bs4 import BeautifulSoup
import sys
import os
import time
from random import randint


input_file :str = "link.txt"
output_file :str = "cmd.sh"
log_file :str = "log.txt"
failed_links :list = []
pre_String :str = r"wget -cr -np -O"

def contentHTML(URL :str):
    """
    Requests html of given links, Times out after 6 seconds;
    Raises Timeout and connection error

    :para: string of URL
    :return: string of HTML page or None
    """
    wait :int  = randint(5, 12)
    time.sleep(wait)
    try:
        content = requests.get(URL, timeout=6)
    except (requests.Timeout, ConnectionError) as e:
        print("Server took too long")
        print(f"ERROR: {e}. Site: {URL}\n")
        failed_links.append(URL)
        return None

    return content.text

def fileCheck() -> bool:
    dir_list : list = os.listdir()

    if input_file not in dir_list:
        print(f"No {input_file} found.\nCreating.......\n")
        open(input_file, "w")
        return False
    else:
        print(f"{input_file} found.\nStarting.........................\n")

    return True

def processLinks() -> list:
    """
    Reads the file of Links and Removes duplicates from the list of links
    Exits the programme if the file is empty

    :para: None
    :return: list of links
    """

    # Read the File: input_file
    with open(input_file, "r") as lf:
        lines = lf.readlines()
        lines = [ x for x in lines if x != "\n" ]

    links_num :int = len(lines)

    if links_num == 0:
        print(f"ERROR: File Empty: {os.getcwd()}/{input_file}")
        sys.exit(0)

    # Remove Duplicated list elements
    lines_filtered :list = []
    [ lines_filtered.append(line) for line in lines if line not in lines_filtered]

    links_list = [ x.strip() for x in lines_filtered]
    return links_list

def scrapingFunc():
    """
    calls func: processLinks
    calls func: contentHTML
    Scrapes the HTML page if Every thing goes ok

    :para: None
    :return: dictionary of list[video_name, video_URL] or None
    """

    links :list = processLinks()
    data_dic = {}

    for index, link in enumerate(links):
        content :str = contentHTML(link)
        if content is None:
            continue

        soup = BeautifulSoup(content, "lxml")

        #inc
        video_block = soup.find("div", {"id": "flix-videowrap"})
        if video_block is None:
            print(f"ERROR: NoneType\nURL: {link}\n")
            failed_links.append(link)
            continue

        name = video_block.find("h2").get_text()
        inc
        embedded_link_block = video_block.find("div", attrs={"id":"flix-stream"})
        embedded_link = embedded_link_block.find("source")["src"]

        embedded_link = embedded_link.replace("//", "http://")
        data_dic[index] = [name, embedded_link]

    if len(data_dic) == 0:
        return None
    return data_dic


def createBashFile(full_dict :dict):
    """
    Parses the given dictionary
    Checks if output file already exists
        if it does: read file content and add those into log file
        If not: Wrap the Name and link into appropriate bash command
    :para: dictionary of list[video_name, video_link]
    :return: None
    """
    dict_values :list = list(full_dict.values())
    dict_last_value :list = dict_values[-1]

    file_check = os.path.exists(output_file)
    if file_check is True:
        with open(output_file, "r") as logging:
            old_lines = logging.read()
            logFile = open(log_file, "a")
            logFile.write(old_lines)
            logFile.write("\nEnd of OLD\n")
            logFile.close()
            print("LOG: New Entry\n")

    with open(output_file, "w") as cmd_f:
        cmd_f.write(r"#! /bin/bash"+ "\n\n")
        for value in dict_values:
            full_str = f"{pre_String} \"{value[0]}.mp4\" \"{value[1]}\""

            cmd_f.write(full_str+"\n")
            if value != dict_last_value:
                sleep_int : int = randint(10, 28)
                sleep_str = f"sleep {sleep_int}s"
                cmd_f.write(sleep_str+"\n")



def main():

    if fileCheck() is not True:
        print("Exiting out of the programme\n")
        sys.exit(0)

    whole_dic = scrapingFunc()
    if whole_dic is not None:
        createBashFile(whole_dic)
    else:
        print("ERROR: Dictionary is Empty\n")

    # If any link have failed
    if len(failed_links) != 0:
        with open("fails.txt", "w") as f:
            for links in failed_links:
                f.write(links + "\n")


if __name__ == "__main__":
    main()

